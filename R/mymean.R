# chi_fisher_p.R

#' Calcula uma média - teste.
#'
#' @param x (`x`) vetor de valores numéricos
#'
#' @return (`numeric`) média calculada
#'
#' @examples
#'
#' mymean(1:10)
#'
#' @export
#'

mymean <- function(x) {
    m <- mean(x, na.rm = TRUE)
    return (m)
}

