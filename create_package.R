
#install.packages("usethis")

# usethis::create_package("X:/GitLab/testrpackage")

usethis::use_package_doc()

usethis::use_roxygen_md()

usethis:::edit_file("DESCRIPTION")

usethis::use_mit_license("Joao Tolentino")

usethis::use_package("emmeans")

usethis::use_r("mymean")

usethis::use_data_raw()

usethis::use_data(x)

usethis::use_r("data")

devtools::document()

usethis::use_testthat()

usethis::use_test()

usethis::use_readme_rmd()

usethis::use_build_ignore(".gitlab-ci.yml")
usethis::use_build_ignore("create_package.R")


